/*
 * MPU6050.h
 *
 *  Created on: 2 mar 2023
 *      Author: fiacovel
 */

/*
abbiamo posto SMPLRT_DIV_SET = 0x07 (7 in decimale)
e CONFIG_SET = 0X00 che significa DLPF_CFG = 0 quindi Fs=8
ora Sample Rate = FS/ (1 + SMPLRT_DIV) = 8/8 = 1
 */
// Initial register values are (UNVARIABLE) and are written at pag 6 (Register Map)of MPU6050 register map
// functions prototype, structures declarations, preprocessors instructions
#ifndef INC_MPU6050_H_  // preprocessors instructions
#define INC_MPU6050_H_
#endif /* INC_MPU6050_H_ */
//#include "main.h"
#include <stm32f4xx_hal.h>
//Register address definition
#define MPU6050_ADDR 0xD0 //I2C address on MPU6050 chip, = 0x68 << 1 (shifto xchè HAL_I2C_Mem_Read solamente i primi 7 su 8, scartando l'ultimo)
#define PWR_MGMT_1_REG 0x6B   // power and clock management  (UNVARIABLE)
#define PWR_MGMT_1_SET 0x00   // power and clock management  // REGISTER USER MUST DEFINE
#define SMPLRT_DIV_REG 0x19    // specifies the divider from the gyroscope output rate used to generate the Sample Rate, (UNVARIABLE)
#define SMPLRT_DIV_SET 0x07       // REGISTER USER MUST DEFINE
#define CONFIG_REG 0X1A        //This register configures the external Frame Synchronization (FSYNC), (UNVARIABLE)
#define CONFIG_SET 0X00       // REGISTER USER MUST DEFINE
#define GYRO_CONFIG_REG 0x1B   //used to trigger gyroscopes self test and configure the gyroscopes’ full scale range, (UNVARIABLE)
#define GYRO_CONFIG_SET 0x00   // REGISTER USER MUST DEFINE
#define ACCEL_CONFIG_REG 0x1C   //used to trigger accelerometer self test and configure the accelerometer full scale range(UNVARIABLE)
#define ACCEL_CONFIG_SET 0x00  // REGISTER USER MUST DEFINE
//#define INT_PIN_CFG_REG 0x37	//This register configures the behavior of the interrupt signals at the INT pins
//#define INT_ENABLE_REG  0x38    //Interrupt enable register
//#define INT_STATUS_REG  0x3A     //Interrupt status enable
#define ACCEL_XOUT_H_REG 0x3B    //The register store the most recent accelerometer measurements on X axes first 8 bits from left
#define ACCEL_XOUT_L_REG 0x3C
#define ACCEL_YOUT_H_REG 0x3D    // .. Y axes
#define ACCEL_YOUT_L_REG 0x3E
#define ACCEL_ZOUT_H_REG 0x3F     // .. Z axes
#define ACCEL_ZOUT_L_REG 0x40
#define TEMP_OUT_H_REG 0x41		  //store the most recent temperature sensor measurement
#define TEMP_OUT_L_REG 0x42
#define GYRO_XOUT_H_REG 0x43	  // These registers store the most recent gyroscope measurements on X axes first 8 bits from left
#define GYRO_XOUT_L_REG 0x44
#define GYRO_YOUT_H_REG 0x45	  // .. Y axes
#define GYRO_YOUT_L_REG 0x46
#define GYRO_ZOUT_H_REG 0x47	  // .. Z axes
#define GYRO_ZOUT_l_REG 0x48
//#define USER_CTRL_REG 0x6A		  //This register allows the user to enable and disable the FIFO buffer, I2C Master Mode, and primary I2C interface
#define WHO_AM_I_REG 0x75    //sensor address on bus I2C of MPU device contains the 6-bit I2C address of the MPU-60X0
#define WAITING_TIME_I2C 1000
#define i2c_timeout 100  //ms
#define Accel_Z_corrector 14418.0f //https://github.com/leech001/MPU6050/blob/master/examples/STM32F401CCU6_MPU6050/Core/Src/mpu6050.c

                                   //Nei programmi,per denotare una costante di tipo float,si può aggiungere 'f' o 'F' finale

typedef struct //structures declaration
{
    int16_t Accel_X_RAW;
    int16_t Accel_Y_RAW;  //formato raw
    int16_t Accel_Z_RAW;
    double Ax;          // valori convertiti
    double Ay;
    double Az;

    int16_t Gyro_X_RAW;
    int16_t Gyro_Y_RAW;
    int16_t Gyro_Z_RAW;

    double Gx;
    double Gy;
    double Gz;

    float Temperature;
}MPU6050_t;


typedef struct //structures declaration
{
	 double Ax;
	 double Ay;
	 double Az;

	double Gx;
	double Gy;
	double Gz;

    double Axmedia;
    double Aymedia;
    double Azmedia;

    double Gxmedia;
    double Gymedia;
    double Gzmedia;
}MPU6050_tmedia;

uint8_t MPU6050_Init(I2C_HandleTypeDef *I2Cx); //preprocessors instructions
void Read_accelerometer(I2C_HandleTypeDef *I2Cx, MPU6050_t *SensorDef); //preprocessors instructions
void Read_temperature(I2C_HandleTypeDef *I2Cx, MPU6050_t *SensorDef); //preprocessors instructions
void Read_Gyro(I2C_HandleTypeDef *I2Cx, MPU6050_t *SensorDef); //preprocessors instructions
