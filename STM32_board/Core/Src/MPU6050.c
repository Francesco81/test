/*
 * MPU6050.c
 *
 *  Created on: 2 mar 2023
 *      Author: fiacovel
 */
#include "MPU6050.h"
//#include "main.h"

/* Riferimenti:
 * https://stackoverflow.com/questions/58808089/how-to-read-data-from-mpu6050-using-stm32f4
 *
 *
 *
 *  */


uint8_t MPU6050_Init(I2C_HandleTypeDef *i2cStructure)
{
    uint8_t check;
    uint8_t Data;

    //Controlliamo l'ID del Device. L'indirizzo del sensore è uguale per tutti i sensori, quello che può cambiare è l'ID.
    // che si trova nel registro WHO I AM
    //HAL_I2C_Mem_Read(struttura I2C, indir sensore MPU, ID, CHECK DEL SENSORE, TIMEOUT)
    HAL_I2C_Mem_Read(i2cStructure, MPU6050_ADDR, WHO_AM_I_REG, 1, &check, 1, WAITING_TIME_I2C); // se il sensore e' presente

    if (check == 104 || check == 105) // 0x68 will be returned by the sensor if everything goes well
    {
        // power management register 0X6B we should write all 0's to wake the sensor up
    	//1 byte è la dimensione del messaggio, i2c_timeout è il tempo di attesa prima che l'invio dei dati sia consider perso
        Data = (uint8_t)PWR_MGMT_1_SET;
        HAL_I2C_Mem_Write(i2cStructure, MPU6050_ADDR, PWR_MGMT_1_REG, 1, &Data, 1, WAITING_TIME_I2C);

        // Set DATA RATE of 1KHz by writing SMPLRT_DIV register ((DLPF_CFG = 1, SMPLRT_DIV=0)
        Data = (uint8_t)SMPLRT_DIV_SET;
        HAL_I2C_Mem_Write(i2cStructure, MPU6050_ADDR, SMPLRT_DIV_REG, 1, &Data, 1, WAITING_TIME_I2C);

        // Set accelerometer configuration in ACCEL_CONFIG Register
        // XA_ST=0,YA_ST=0,ZA_ST=0, AFS_SEL=0 ->  Full Scale Range = +/-2g
        Data = (uint8_t)ACCEL_CONFIG_SET; // 00 setta il range 2g
        HAL_I2C_Mem_Write(i2cStructure, MPU6050_ADDR, ACCEL_CONFIG_REG, 1, &Data, 1, WAITING_TIME_I2C);

        // Set Gyroscopic configuration in GYRO_CONFIG Register
        // XG_ST=0,YG_ST=0,ZG_ST=0, FS_SEL=0 ->  +/-250 °/s
        Data = (uint8_t)GYRO_CONFIG_SET;
        HAL_I2C_Mem_Write(i2cStructure, MPU6050_ADDR, GYRO_CONFIG_REG, 1, &Data, 1, WAITING_TIME_I2C);

        Data = (uint8_t)CONFIG_SET;
        HAL_I2C_Mem_Write(i2cStructure, MPU6050_ADDR, CONFIG_REG, 1, &Data, 1, WAITING_TIME_I2C);

        return 0; //usato nella funzione main determina la fine dell'esecuzione del programma
    }             //Il programma termina restituendo il valore 0 ed indica solo che il programma è terminato correttamente
    return 1;    //significa che c'è qualche errore

}


void Read_accelerometer(I2C_HandleTypeDef *i2cStructure, MPU6050_t *SensorDef)// (strutti2c, struttura sensore)
{
	uint8_t Data_readed[6];  //Par 4.17 Registers 59 to 64 – Accelerometer Measurements register document
	                         //ACCEL_XOUT_H, ACCEL_XOUT_L, ACCEL_YOUT (da pag19 datasheet si vede che MPU invia prima MSB(most significant bit) poi LSB)->big endian
	HAL_I2C_Mem_Read(i2cStructure, MPU6050_ADDR, ACCEL_XOUT_H_REG, 1, Data_readed, 6, WAITING_TIME_I2C); // 6 registri letti 1 byte alla volta
    //ACCEL_XOUT_H_REG è l'indirizzo del primo registro che voglio leggere xchè ha indir piu basso, il 6 sono i byte da leggere
	SensorDef->Accel_X_RAW = (uint16_t)(Data_readed[0] << 8 | Data_readed[1]);  //is a cast to a 16 bit variable, that put together 2 byte
	SensorDef->Accel_Y_RAW = (uint16_t)(Data_readed[2] << 8 | Data_readed[3]);  // la parte più significativa corrisponde al byte più significativo letto nell'accelerometro
	SensorDef->Accel_Z_RAW = (uint16_t)(Data_readed[4] << 8 | Data_readed[5]);  //e la parte meno significativa corrisponde esattamente all'altro byte 
    // see pag 29 register map

	SensorDef->Ax = SensorDef->Accel_X_RAW /16384.0; //equivale a (*SensorDef).Ax = (*SensorDef).Accel_X_RAW/16384.0;
	SensorDef->Ay = SensorDef->Accel_Y_RAW /16384.0; // g
	SensorDef->Az = SensorDef->Accel_Z_RAW /Accel_Z_corrector;

}

void Read_Gyro(I2C_HandleTypeDef *i2cStructure, MPU6050_t *SensorDef)// (strutti2c, struttura sensore)
{
	uint8_t Data_readed[6];  //Par 4.17 Registers 59 to 64 – Accelerometer Measurements register document
	                         //ACCEL_XOUT_H, ACCEL_XOUT_L, ACCEL_YOUT (da pag19 datasheet si vede che MPU invia prima MSB(most significant bit) poi LSB)->big endian
						    //per questo dopo shifto di 8 bit (<< 8)
	HAL_I2C_Mem_Read(i2cStructure, MPU6050_ADDR, GYRO_XOUT_H_REG, 1, Data_readed, 6, WAITING_TIME_I2C); // 6 registri letti 1 byte alla volta
    //ACCEL_XOUT_H_REG è l'indirizzo del primo registro che voglio leggere xchè ha indir piu basso, il 6 sono i byte da leggere
	SensorDef->Gyro_X_RAW = (uint16_t)(Data_readed[0] << 8 | Data_readed[1]);  //is a cast to a 16 bit variable, that put together 2 byte
	SensorDef->Gyro_Y_RAW = (uint16_t)(Data_readed[2] << 8 | Data_readed[3]);  // la parte più significativa corrisponde al byte più significativo letto nell'accelerometro
	SensorDef->Gyro_Z_RAW = (uint16_t)(Data_readed[4] << 8 | Data_readed[5]);  //e la parte meno significativa corrisponde esattamente all'altro byte 
    // see pag 29 register map

	SensorDef->Gx = SensorDef->Gyro_X_RAW /131.0; //dps ( °/s )
	SensorDef->Gy = SensorDef->Gyro_Y_RAW /131.0;
	SensorDef->Gz = SensorDef->Gyro_Z_RAW /131.0;

}

void Read_temperature(I2C_HandleTypeDef *i2cStructure, MPU6050_t *SensorDef) //i2c bus structure, definition sensor values
{
	uint8_t Data_readed[2];
	int16_t temp;

	HAL_I2C_Mem_Read(i2cStructure, MPU6050_ADDR, TEMP_OUT_H_REG, 1, Data_readed, 2, WAITING_TIME_I2C);
	temp = (float)(Data_readed[0] << 8 | Data_readed[1]); //is a cast to a 16 bit variable, that put together 2 byte
	SensorDef-> Temperature = (float)((int16_t)temp/(float)340.0 + (float)36.53); // conversion temperature values pag 30 MPU6050 REGISTER

}

//
