/*
 * MPU6050.h
 *
 *  Created on: 2 mar 2023
 *      Author: fiacovel
 */
// functions prototype, structures declarations, preprocessors instructions
#ifndef INC_MPU6050_H_  // preprocessors instructions
#define INC_MPU6050_H_

//Register address definition
#define MPU6050_ADDR 0xD0 //I2C address on MPU6050 chip, = 0x68 << 1 (shifto xchè HAL_I2C_Mem_Read solamente i primi 7, scartando l'ultimo)

#define PWR_MGMT_1_REG 0x6B   // power and clock management
#define PWR_MGMT_1_SET 0x00   // power and clock management
#define SMPLRT_DIV_REG 0x19    // specifies the divider from the gyroscope output rate used to generate the Sample Rate
#define SMPLRT_DIV_SET 0x07
#define CONFIG_REG 0X1A        //This register configures the external Frame Synchronization (FSYNC)
#define CONFIG_SET 0X00
#define GYRO_CONFIG_REG 0x1B   //used to trigger gyroscopes self test and configure the gyroscopes’ full scale range
#define GYRO_CONFIG_SET 0x00
#define ACCEL_CONFIG_REG 0x1C   //used to trigger accelerometer self test and configure the accelerometer full scale range
#define ACCEL_CONFIG_SET 0x00
//#define INT_PIN_CFG_REG 0x37	//This register configures the behavior of the interrupt signals at the INT pins
//#define INT_ENABLE_REG  0x38    //Interrupt enable register
//#define INT_STATUS_REG  0x3A     //Interrupt status enable
#define ACCEL_XOUT_H_REG 0x3B    //The register store the most recent accelerometer measurements on X axes first 8 bits from left
#define ACCEL_XOUT_L_REG 0x3C
#define ACCEL_YOUT_H_REG 0x3D    // .. Y axes
#define ACCEL_YOUT_L_REG 0x3E
#define ACCEL_ZOUT_H_REG 0x3F     // .. Z axes
#define ACCEL_ZOUT_L_REG 0x40
#define TEMP_OUT_H_REG 0x41		  //store the most recent temperature sensor measurement
#define TEMP_OUT_L_REG 0x42
#define GYRO_XOUT_H_REG 0x43	  // These registers store the most recent gyroscope measurements on X axes first 8 bits from left
#define GYRO_XOUT_L_REG 0x44
#define GYRO_YOUT_H_REG 0x45	  // .. Y axes
#define GYRO_YOUT_L_REG 0x46
#define GYRO_ZOUT_H_REG 0x47	  // .. Z axes
#define GYRO_ZOUT_l_REG 0x48
//#define USER_CTRL_REG 0x6A		  //This register allows the user to enable and disable the FIFO buffer, I2C Master Mode, and primary I2C interface
#define WHO_AM_I_REG 0x75    //sensor address on bus I2C of MPU device contains the 6-bit I2C address of the MPU-60X0
#define WAITING_TIME_I2C 1000
#define i2c_timeout 100  //ms
#define Accel_Z_corrector 14418.0f

#endif /* INC_MPU6050_H_ */

#include "main.h"

typedef struct //structures declaration
{
    int16_t Accel_X_RAW;
    int16_t Accel_Y_RAW;  //formato raw
    int16_t Accel_Z_RAW;
    double Ax;          // valori convertiti
    double Ay;
    double Az;

    int16_t Gyro_X_RAW;
    int16_t Gyro_Y_RAW;
    int16_t Gyro_Z_RAW;

    double Gx;
    double Gy;
    double Gz;

    float Temperature;
}MPU6050_t;

uint8_t MPU6050_Init(I2C_HandleTypeDef *I2Cx); //preprocessors instructions
void Read_accelerometer(I2C_HandleTypeDef *I2Cx, MPU6050_t *SensorDef); //preprocessors instructions
void Read_temperature(I2C_HandleTypeDef *I2Cx, MPU6050_t *SensorDef); //preprocessors instructions
